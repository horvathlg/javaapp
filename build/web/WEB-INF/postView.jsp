<%-- 
    Document   : postView
    Created on : 2014.04.18., 15:27:22
    Author     : horvathlg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Posts</title>
    </head>
    <body>
        <h1>Posts</h1>
        <h2>${postTitle}</h2>
        <p>${postBody}</p>
    </body>
</html>
