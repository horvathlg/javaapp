<%-- 
    Document   : postFormView
    Created on : 2014.04.18., 15:30:05
    Author     : horvathlg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create Post</title>
    </head>
    <body>
        <h1>Create Post:</h1>
        <spring:nestedPath path="post">
            <form action="" method="post">
                Title:
                <spring:bind path="postTitle">
                    <input type="text" name="${status.expression}" value="${status.value}">
                </spring:bind>
                 Body:
                <spring:bind path="postValue">
                    <input type="textarea" name="${status.expression}" value="${status.value}">
                </spring:bind>
                <input type="submit" value="OK">
            </form>
        </spring:nestedPath>
    </body>
</html>
