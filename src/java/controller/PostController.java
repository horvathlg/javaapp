/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import service.PostService;

/**
 *
 * @author horvathlg
 */
public class PostController extends SimpleFormController {
    
    public PostController() {
        //Initialize controller properties here or 
        //in the Web Application Context

        setCommandClass(Post.class);
        setCommandName("post");
        setSuccessView("postView");
        setFormView("postFormView");
    }
    
    
     @Override
     protected ModelAndView onSubmit(
            HttpServletRequest request, 
            HttpServletResponse response, 
            Object command, 
            BindException errors) throws Exception {
         
        Post post = (Post) command; 
        ModelAndView mv = new ModelAndView(getSuccessView());
        mv.addObject("postTitle", PostService.postBody(post.getTitle()));
        mv.addObject("postBody", PostService.postTitle(post.getBody()));
        return mv;
     }
     
     private PostService postService;
     
     public void setPostService(PostService postService){
         this.postService = postService;
     }
}
